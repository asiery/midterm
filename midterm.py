#!/usr/bin/env python3

import findspark
from pyspark import *
import string
import enchant
import sys
import random
import googlesearch

findspark.init()
#This method is implmented to use pyspark even though it isn't on the sys.path by default

conf = SparkConf().setMaster("local").setAppName("Midterm")
sc = SparkContext(conf=conf)

alphabet = 'abcdefghijklmnopqrstuvwxyz'
#Initiliazing all letters of the alaphabet

dictionary = enchant.Dict('en-US')
#Defining the dictionary we will use


def LetterDifference(letter, compareTo='e'):
    return string.ascii_lowercase.index(compareTo.lower()) - string.ascii_lowercase.index(letter.lower())
#This function is used to compare all letters with e which is the most common letter


def shiftText(text, shifter):
    newString = ""
    for char in text:
        if char.lower() not in alphabet:
            newString += char
        else:
            newIndex = string.ascii_lowercase.index(char.lower()) + shifter
            if newIndex > 25:  #loop back around to beginning of alphabet
                newIndex -= 26
            if newIndex < 0:  #loop back around to beginning of alphabet
                newIndex += 26
            newString += alphabet[newIndex]
    return newString

#This method will determine how many letters must be shifted over


def mostCommonLetter(charArray, index=0):
    try:
        char = charArray[index][0]  #first character in sorted array is the most common character
        if str(char) not in alphabet:  #if most common character is not a letter, find the second most common.
            return mostCommonLetter(charArray, index + 1)
        return char
    except Exception as e:
        print(e)
        sys.exit(0)
    return None


def WordCheck(decrypted_words):
    passed = 0
    failed = 0
    test_count = int(len(decrypted_words) / 20)
    for _ in range(test_count):
        index = random.randint(0, len(decrypted_words) - 1)
        word = ''.join(c for c in decrypted_words[index] if str(c).isalnum()).lower()#method returns True if all characters in the string are alphanumeric and then will lower true values
        if not word:
            test_count -= 1
            continue
        if dictionary.check(word):
            passed += 1
        else:
            failed += 1
    if failed == 0:
        print('Tested {} word(s): 100% validity.'.format(test_count))
        return True
    passed_percentage = (passed / test_count) * 100
    print('Tested {} word(s): {}% validity.'.format(test_count, "{0:.2f}".format(passed_percentage)))
    if passed_percentage >= 75:
        return True
    return False


def check_google(text):
    query_length = min(20, len(text))
    query_text = ""
    for i in range(query_length):
        query_text += text[i] + " "
    search_results = googlesearch.search(query=query_text, tld="com", lang='en', num=3, stop=3, pause=2.0)
    if search_results:
        print("Potential sources:")
        for result in search_results:
            print(result)
#This method will find the source of the text


filename = input("Which of the text files would you like to decrypt?: Encrypted-1, Encrytped-2, or Encrypted-3? ")
auto_check_complete = False
res = input("Would you like code to automatically check decrypted text (auto) or decide for yourself (manual)? ")
if res.lower().startswith('a'):
    auto_check_complete = True

textFile = sc.textFile('{}.txt'.format(filename)).cache()
chars = textFile.flatMap(lambda line: list(line))
charMap = chars.map(lambda char: (char.lower(), 1)).reduceByKey(lambda k, v: k + v).collect()
charMap.sort(key=lambda charTuple: charTuple[1], reverse=True)
letter = mostCommonLetter(charMap)

letterMarkers = ['e', 't', 'a', 'o', 'i', 'n', 's', 'h', 'r', 'd']  # Most frequent letters and order
letterMarkerIndex = 0
validWords = False
print("The most common letter detected was: {}".format(letter))
while not validWords:
    if letterMarkerIndex > len(letterMarkers) - 1:
        print("Exhausted the 10 most common letters. Quitting...")
        sys.exit(0)
    letterMarker = letterMarkers[letterMarkerIndex]
    diff = LetterDifference(letter, letterMarker)
    print("Adjusting for '{}'. Shifting text by {}...".format(letterMarker, diff))
    adjustedText = shiftText(chars.collect(), diff)
    print(adjustedText)
    if auto_check_complete:
        adjustedWords = adjustedText.split()
        if WordCheck(adjustedWords):
            validWords = True
            print("Most of the words appear in the dictionary.")
    else:
        res = input("Does the output appear to be English? yes or no: ")
        adjustedWords = adjustedText.split()
        if res.lower().startswith('y'):
            validWords = True
    letterMarkerIndex += 1
with open("{}_decrypted.txt".format(filename), 'w+') as f:  # save decrypted text to a file
    f.writelines(adjustedText)
    f.close()
print("Decrypted file saved: {}_decrypted.txt".format(filename))
check_google(adjustedWords)  # search for the text source on Google